## create route53 record 

resource "aws_route53_record" "example" {
  zone_id = "Z04799832H0G9A5UCTJVV"
  name    = "example"
  type    = "A"

    alias {
      name                   = aws_alb.alb.dns_name
      zone_id                 = aws_alb.alb.zone_id
      evaluate_target_health = true
    }
  }

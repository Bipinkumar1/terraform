  # create vpc
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "Bipin-vpc-01"
  }
}

## create public subnet 01
resource "aws_subnet" "public_01" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.public-subnet_01
  availability_zone = "ap-south-1a"

  tags = {
    Name = "Bipin-pub-sub-01"
  }
}


## create private subnet 01
resource "aws_subnet" "private_01" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private-subnet_01
  availability_zone = "ap-south-1b"

  tags = {
    Name = "Bipin-priv-sub-01"
  }
}


## create internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "Bipin-igw-01"
  }
}

## create nat gateway
resource "aws_eip" "nat_gateway" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_gateway.id
  subnet_id     = aws_subnet.public_01.id

  tags = {
    "Name" = "Bipin-nat-01"
  }
}

## create  public route table
resource "aws_route_table" "example" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "Bipin-route-pub-01"
  }
}

## create private route table
resource "aws_route_table" "example_1" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }

  tags = {
    Name = "Bipin-route-priv-01"
  }
}


## public subnet association with route table
resource "aws_route_table_association" "public-instance" {
  subnet_id      = aws_subnet.public_01.id
  route_table_id = aws_route_table.example.id
}


## private subnet association with route table
resource "aws_route_table_association" "private-instance" {
  subnet_id      = aws_subnet.private_01.id
  route_table_id = aws_route_table.example_1.id
}







output "public-ec2-1-ip" {
  value = aws_instance.public_instance_01[0].public_ip
}

output "public-ec2-2-ip" {
  value = aws_instance.public_instance_01[1].public_ip
}


output "private-ec2-1-ip" {
  value = aws_instance.private_instance_01[0].private_ip
}

output "private-ec2-2-ip" {
  value = aws_instance.private_instance_01[1].private_ip
}

output "public-instance-sg-id" {
  value = aws_security_group.public_instance_sg.id
}

output "private-instance-sg-id" {
  value = aws_security_group.private_instance_sg.id
}


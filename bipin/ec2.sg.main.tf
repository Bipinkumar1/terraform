# Creating Security group for public instance

resource "aws_security_group" "public_instance_sg" {
  description = "Allow all ports to private instance"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "allow all ports"
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "allow ssh to local"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "allow ssh to local"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }



  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.security_group_public
  }
}


#Security group for Private Instances

resource "aws_security_group" "private_instance_sg" {
  description = "Allow ssh for private instance"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "allow all ports"
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = var.security_group_priv
  }
}


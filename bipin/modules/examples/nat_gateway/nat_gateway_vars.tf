variable "subnet_id" {
  type = string
  description = "describe your vpc_id"
}

variable "nat_gateway_name" {
  type = string
  description = "nat gateway name"
  default = "coustom_nat_gateway"
}


variable "vpc_id" {
  type = string
  description = "describe your vpc_id"
}

variable "public_route_table_name" {
  type = string
  description = "custom subnet name"
  default = "my-coustom-public_route_table"
}

variable "internet_gateway_id" {
  type = string
  description = "describe your internet_gateway_id"
}

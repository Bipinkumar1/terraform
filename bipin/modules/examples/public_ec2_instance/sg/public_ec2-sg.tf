# Creating Security group for public instance

resource "aws_security_group" "public_instance_sg" {
  description = "Allow all ports to private instance"
  vpc_id      = var.vpc_id

  ingress {
    description = "allow all ports"
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = var.ingress_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = var.egress_cidr_blocks
  }

  tags = {
    Name = var.public_security_group_name
  }
}

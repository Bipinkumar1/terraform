variable "public_ubuntu_ami_id" {
  type = string
  description = "describe your public ubuntu ami id"
}


variable "key_name" {
  type = string
  description = "describe your aws key"
}

variable "instance_type" {
  type = string
  description = "describe your instance type"
}

variable "availability_zone" {
  type = string
  description = "describe your availability zone"
}

variable "subnet_id" {
  type = string
  description = "describe your public subnet id"
}

variable "instance_no" {
  type = string
  description = "describe your instance no."
}

variable "public_instance_name" {
  type = string
  description = "describe your instance name"
}

variable "vpc_security_group_ids" {
  type = list
  description = "describe your vpc security group id"
}


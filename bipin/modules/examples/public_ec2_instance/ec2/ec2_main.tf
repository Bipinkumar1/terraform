# Launching  public ec2 Instances


resource "aws_instance" "public_instance" {
  ami                    = var.public_ubuntu_ami_id
  key_name               = var.key_name
  instance_type          = var.instance_type
  vpc_security_group_ids = var.vpc_security_group_ids
  availability_zone      = var.availability_zone
  subnet_id              = var.subnet_id
  associate_public_ip_address  = true
  count                  = var.instance_no

  tags = {
    Name = "${var.public_instance_name}-${count.index + 1}"
  }
}



  # ## create public subnet 

resource "aws_subnet" "public" {
  vpc_id            = var.vpc_id
  cidr_block        = var.public_subnet_block
  availability_zone = var.availability_zone

  tags = {
    Name = var.subnet_name
  }
}



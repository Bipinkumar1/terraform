variable "vpc_id" {
  type = string
  description = "describe your vpc_id"
}

variable "public_subnet_block" {
  type = string
  description = "subnet cidr range"
}

variable "availability_zone" {
  type = string
  description = "subnet availability zone"
}

variable "subnet_name" {
  type = string
  description = "custom subnet name"
  default = "my-coustom-subnet"
}


variable "vpc_id" {
  type = string
  description = "describe your vpc_id"
}

variable "private_route_table_name" {
  type = string
  description = "custom subnet name"
  default = "my-coustom-private_route_table"
}

variable "nat_gateway_id" {
  type = string
  description = "describe your internet_gateway_id"
}

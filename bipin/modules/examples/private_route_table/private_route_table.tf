## create private route table

resource "aws_route_table" "nat" {
  vpc_id = var.vpc_id

  route {
    cidr_block     = "0.0.0.0/0"
    gateway_id = var.nat_gateway_id
  }

  tags = {
    Name = var.private_route_table_name
  }
}

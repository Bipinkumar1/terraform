resource "aws_route_table_association" "public" {
  subnet_id      = var.private_subnet_id
  route_table_id = var.private_route_table_id
}

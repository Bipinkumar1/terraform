# create vpc

module "vpc" {
  source = "../VPC"

  vpc_name = "Bipin-custom-vpc"
  vpc_cidr = "10.0.0.0/16"
  
}

## create public subnet 

module "public_subnets" {
  source = "../public_subnet"

  vpc_id              = module.vpc.vpc_id
  public_subnet_block = "10.0.1.0/24"
  availability_zone   = "ap-south-1a"
  subnet_name         = "Bipin-public-subnet"
}


## create private subnet 

module "private_subnets" {
  source = "../private_subnet"

  vpc_id              = module.vpc.vpc_id
  private_subnet_block = "10.0.2.0/24"
  availability_zone   = "ap-south-1b"
  subnet_name         = "Bipin-private-subnet"
}


## create internet gateway
module "internet_gateway" {
  source = "../internet_gateway"
  
  vpc_id                 = module.vpc.vpc_id
  internet_gateway_name  = "Bipin_Igw"


}


## create nat gateway
module "nat_gateway" {
  source  = "../nat_gateway"

  subnet_id          = module.public_subnets.public_subnet_id
  nat_gateway_name   = "Bipin_nat_gateway"

}

## create public route table 
module "aws_route_table_public" {
  source  = "../public_route_table"

  vpc_id                  = module.vpc.vpc_id
  internet_gateway_id     = module.internet_gateway.internet_gateway_id
  public_route_table_name = "Bipin_public_route_table"

}

## create private route table 
module "aws_route_table_private" {
  source  = "../private_route_table"

  vpc_id                  = module.vpc.vpc_id
  nat_gateway_id          = module.nat_gateway.nat_gateway_id
  private_route_table_name = "Bipin_private_route_table"

}

## ## private subnet association with route table
module "aws_route_table_association_private" {
  source  = "../subnet_association_with_route_table/private"

  private_subnet_id      = module.private_subnets.private_subnet_id
  private_route_table_id = module.aws_route_table_private.private_route_table_id

}

## ## public subnet association with route table
module "aws_route_table_association_public" {
  source  = "../subnet_association_with_route_table/public"

  public_subnet_id      = module.public_subnets.public_subnet_id
  public_route_table_id = module.aws_route_table_public.public_route_table_id

}


## create public instance sg
module "public_ec2_instance_sg" {
  source = "../public_ec2_instance/sg"

  vpc_id                        = module.vpc.vpc_id
  public_security_group_name    = "Bipin_public_ec2_sg"
  ingress_cidr_blocks           = ["0.0.0.0/0"]
  egress_cidr_blocks            = ["0.0.0.0/0"]

}



## create public ec2 instance
module "public_ec2_instance" {
  source  = "../public_ec2_instance/ec2"

  public_ubuntu_ami_id    = "ami-071932ed8de01b802"
  key_name                = "AWS-Key"
  instance_type           = "t2.micro"
  availability_zone       =  "ap-south-1a"
  public_instance_name    = "Bipin_public_ec2"
  subnet_id               =  module.public_subnets.public_subnet_id
  vpc_security_group_ids  = [module.public_ec2_instance_sg.public-instance-sg-id]
  instance_no             = 2
}


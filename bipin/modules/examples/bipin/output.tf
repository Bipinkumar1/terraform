output "vpc_id" {
  value = module.vpc.vpc_id
}

output "public_subnet_id" {
  value = module.public_subnets.public_subnet_id
}


output "private_subnet_id" {
  value = module.private_subnets.private_subnet_id
}

output "internet_gateway_id" {
  value = module.internet_gateway.internet_gateway_id
}

output "nat_gateway_id" {
  value = module.nat_gateway.nat_gateway_id
}

output "public_route_table_id" {
  value = module.aws_route_table_public.public_route_table_id
}

output "private_route_table_id" {
  value = module.aws_route_table_private.private_route_table_id
}

output "public_ec2_1st_instance_ip" {
  value = module.public_ec2_instance.public_ec2_1st_instance_ip
}                                                                                                                                      

output "public_ec2_2nd_instance_ip" {
  value = module.public_ec2_instance.public_ec2_2nd_instance_ip
} 

output "public-instance-sg-id" {
  value = module.public_ec2_instance_sg.public-instance-sg-id
} 

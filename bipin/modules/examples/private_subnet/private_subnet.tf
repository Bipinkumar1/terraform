## create private subnet 

resource "aws_subnet" "private" {
  vpc_id            = var.vpc_id
  cidr_block        = var.private_subnet_block
  availability_zone = var.availability_zone
  
  tags = {
    Name = var.subnet_name
  }
}


variable "vpc_id" {
  type = string
  description = "describe your vpc_id"
}

variable "internet_gateway_name" {
  type = string
  description = "internet gateway name"
  default = "coustom_internet_gateway"
}


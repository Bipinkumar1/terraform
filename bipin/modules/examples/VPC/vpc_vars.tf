variable "vpc_name" {
  type = string
  description = "custom vpc name"
  default = "my-coustom-vpc"
}

variable "vpc_cidr" {
  type = string
  description = "The IP range to use for the vpc"
  default = "10.0.0.0/24"
}



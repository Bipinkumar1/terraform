##VPC resources
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}


variable "public-subnet_01" {
  description = "pubic_subnet-01"
  default     = "10.0.1.0/24"
}

variable "private-subnet_01" {
  description = "private_subnet-01"
  default     = "10.0.2.0/24"
}


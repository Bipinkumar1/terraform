# Launching  public ec2 Instances


resource "aws_instance" "public_instance_01" {
  ami                    = var.Ubuntu_ami
  key_name               = "AWS-Key"
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.private_instance_sg.id]
  availability_zone      = "ap-south-1a"
  subnet_id              = aws_subnet.public_01.id
  associate_public_ip_address  = true
  count                  = 2

  tags = {
    Name = "${var.public-instance_1}-${count.index + 1}"
  }
}


# Launching Private  ec2 Instance -2

resource "aws_instance" "private_instance_01" {
  ami                    = var.Ubuntu_ami
  key_name               = "AWS-Key"
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.private_instance_sg.id]
  availability_zone      = "ap-south-1b"
  subnet_id              = aws_subnet.private_01.id
  count                  = 2

  tags = {
    Name = "${var.priv-instance_1}-${count.index + 1}"
  }
}

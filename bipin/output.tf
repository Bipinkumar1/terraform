output "vpc_id" {
  value = aws_vpc.main.id
}


output "public-subnet-1" {
  value = aws_subnet.public_01.id
}

output "private-subnet-1" {
  value = aws_subnet.private_01.id
}

output "internet-gateway-id" {
  value = aws_internet_gateway.igw.id
}

output "nat-gateway-id" {
  value = aws_nat_gateway.nat_gateway.id
}

output "private-route-table-id" {
  value = aws_route_table.example_1.id
}

output "public-route-table-id" {
  value = aws_route_table.example.id
}


## Create a  application load balancer:

resource "aws_alb" "alb" {
  name            = "Bipin-alb"
  security_groups = [aws_security_group.ALB.id]
  subnets         = [aws_subnet.public_01.id,aws_subnet.private_01.id]

  tags = {
   Name = "Bipin-alb"
  }
  

}

#Cteate Target Group

resource "aws_lb_target_group" "test" {
  name     = "Bipin-target-group"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id

  tags = {
   Name = "Bipin-target-group"
  }
  
}

## Create listener

resource "aws_alb_listener" "listener_http" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.test.arn
    type             = "forward"
  }
}

## Attach instances to listener

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.test.arn
  target_id        = aws_instance.public_instance_01[0].id
  port             = 8080
}

resource "aws_lb_target_group_attachment" "test_1" {
  target_group_arn = aws_lb_target_group.test.arn
  target_id        = aws_instance.public_instance_01[1].id
  port             = 8080
}




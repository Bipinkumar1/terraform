# #Security group for ALB

# resource "aws_security_group" "ALB" {
#   description = "Allow 80 or 8080 ports"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     description = "allow all ports"
#     from_port   = 80
#     to_port     = 80
#     protocol    = "ALL"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     description = "allow all ports"
#     from_port   = 8080
#     to_port     = 8080
#     protocol    = "ALL"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   egress {
#     from_port   = 0
#     to_port     = 65535
#     protocol    = "ALL"
#     cidr_blocks = ["0.0.0.0/0"]
#   }


#   tags = {
#     Name = "Bipin-alb-security-group"
#   }
# }

resource "aws_security_group" "ALB" {
  description = "Allow ssh for private instance"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "allow all ports"
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "ALL"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "Bipin-alb-security-group"
  }
}


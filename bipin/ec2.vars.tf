variable "priv-instance_1" {
  default = "Bipin-priv-instance"
}


variable "public-instance_1" {
  default = "Bipin-public-instance"
}

variable "Ubuntu_ami" {
  default = "ami-071932ed8de01b802"
}


variable "security_group_public" {
  default = "Bipin_sg_public_instance"
}

variable "security_group_priv" {
  default = "Bipin_sg_private_instance"
}